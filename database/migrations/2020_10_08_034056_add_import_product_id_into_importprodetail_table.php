<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddImportProductIdIntoImportprodetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('importprodetails', function (Blueprint $table) {
            $table->foreignId('importprodetail_id')->nullable()->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('importprodetails', function (Blueprint $table) {
            $table->dropColumn('importprodetail_id');
        });
    }
}
