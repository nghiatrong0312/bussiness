<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Session;

class Cartimport
{
    public $product_name    = 0;
    public $product_price   = 0;
    public $wholesale_price = 0;
    public $quantity        = 0;
    public $id_product      = '';
    function addCartimport($id, $arrData, $quantity, $price)
    {

        foreach ($arrData as $key => $value) {
            if ($price) {
                $product_price = $price;
            }elseif($price == '') {
                $product_price = $value['price_product'];
            }
            $product_name     = $value['name_product'];
            $wholesale_price  = $value['wholesale_price'];
        }
        if (empty(Session::has('cartimport'))) {
            $cartimport[$id] = array(
                'id_product'      => $id,
                'product_name'    => $product_name,
                'product_price'   => $product_price,
                'wholesale_price' => $wholesale_price,
                'amount' => $quantity,
            );
        }else{
            $cartimport = Session::get('cartimport');
            if (array_key_exists($id, $cartimport)) {
                $cartimport[$id] = array(
                    'id_product'    => $id,
                    'product_name'  => $product_name,
                    'product_price' => $product_price,
                    'wholesale_price' => $wholesale_price,
                    'amount'        => $cartimport[$id]['amount']+$quantity,
                );
            }else{
                $cartimport[$id] = array(
                    'id_product'    => $id,
                    'product_name' => $product_name,
                    'product_price' => $product_price,
                    'wholesale_price' => $wholesale_price,
                    'amount' => $quantity,
                );
            }
        }
        Session::put('cartimport', $cartimport);
    }
    public function destroyCartimport($id)
    {
        if (!empty(Session::get('cartimport'))) {
            $cartimport = Session::get('cartimport');
            unset($cartimport[$id]);
            Session::put('cartimport', $cartimport);
        }
    }
}
