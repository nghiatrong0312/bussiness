<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Category;

class Product extends Model
{
    protected $table = 'products';
    protected $guarded = [];

    public static function getProducts()
    {
        return $products = Product::all();
    }

    public function getProductsbyID($id)
    {
        return Product::find($id);
    }

    public function getCategories()
    {
        return Category::all();
    }
    public function getCategorybyID()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }
}
