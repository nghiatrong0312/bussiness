<?php

namespace App;

use Session;

class Cart
{
     
    public $product_name    = 0;
    public $product_price   = 0;
    public $amount          = 0;
    public $id_product      = '';
    function addCart($id, $arrData, $amount, $price)
    {
        
        foreach ($arrData as $key => $value) {
            if ($price) {
                $product_price = $price;
            }elseif($price == '') {
                $product_price = $value['price_product'];
            }
            $product_name = $value['name_product'];
        }
        if (empty(Session::has('cart'))) {
            $cart[$id] = array(
                'id_product'    => $id,
                'product_name'  => $product_name,
                'product_price' => $product_price,
                'amount' => $amount,
            );
        }else{
            $cart = Session::get('cart');
            if (array_key_exists($id, $cart)) {
                $cart[$id] = array(
                'id_product'    => $id,
                'product_name' => $product_name,
                'product_price' => $product_price,
                'amount' => $cart[$id]['amount']+$amount,
                );
            }else{
                $cart[$id] = array(
                    'id_product'    => $id,
                    'product_name' => $product_name,
                    'product_price' => $product_price,
                    'amount' => $amount,
                );
            }
        }
    Session::put('cart', $cart);
    }
    public function destroyCart($id)
    {
        if (!empty(Session::get('cart'))) {
            $cart = Session::get('cart');
            unset($cart[$id]);
            Session::put('cart', $cart);
        }
    }
}

