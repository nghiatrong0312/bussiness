<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Customer;

class Order extends Model
{
    protected $table = 'orders';
    protected $guarded = [];

    public function getNameCustomer()
    {
        return $this->belongsto(Customer::class, 'customer_id', 'id');
    }
}
