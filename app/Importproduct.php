<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Importproduct extends Model
{
    protected $table = 'importproducts';
    protected $guarded = [];
}
