<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Importprodetail extends Model
{
    protected $table = 'importprodetails';
    protected $guarded = [];

    public function getNameProduct()
    {
        return $this->belongsto(Product::class, 'product_id', 'id');
    }
}
