<?php

namespace App\Http\Controllers;

use App\Importprodetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RevenueImportController extends Controller
{
    public function index()
    {
        return view('revenue.import.index');
    }

    public function show(Request $request)
    {
        $show_data = '';
        $date = $request->month;
        if($request->day != null){
            $date = $request->month.'-'.$request->day;
        }
        $datas = DB::table('importprodetails')
            ->select(DB::raw('products.wholesale_price, products.name_product, SUM(importprodetails.quantity) as total_quantity'))
            ->leftjoin('products', 'importprodetails.product_id', '=', 'products.id')
            ->whereDATE('importprodetails.created_at', 'like', '%'.$date.'%')
            ->groupBy('importprodetails.product_id')
            ->groupBy('products.name_product')
            ->groupBy('products.wholesale_price')
            ->get();
        foreach ($datas as $data)
        {
            $show_data .= '
            <tr>
               <td>'.$data->name_product.'</td>
               <td>'.$data->total_quantity.'</td>
               <td>'.number_format($data->total_quantity*$data->wholesale_price, 0, ',', '.').'</td>
            </tr>
        ';
        }

        return $show_data;
    }
}
