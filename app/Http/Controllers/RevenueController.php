<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Customer;
use App\Order;

class RevenueController extends Controller
{
    public function order()
    {
        $customer_datas = Customer::All();

        return view('revenue/order', [
            'customer_datas' => $customer_datas,
        ]);
    }

    public function showorder(Request $request)
    {
        $show_data = '';
        $date = $request->month;
        if($request->day != null){
            $date = $request->month.'-'.$request->day;
        }
        if($request->id_customer != 0){
            $id_customer    = $request->id_customer;
            $datas = Order::whereHas('getNameCustomer', function ($query) use ($id_customer,$date) {
                $query->where(['orders.customer_id'=> $id_customer])
                        ->whereDATE('orders.created_at', 'like', '%'.$date.'%');
            })->get();
            foreach($datas as $data){
                $show_data .= '
                    <tr>
                        <td>'.$data->getNameCustomer->name.'</td>
                        <td>'.$data['total_amount'].'</td>
                        <td>'.number_format($data['total_price'], 0, ',', '.').'</td>
                        <td>'.$data['created_at'].'</td>
                        <td>
                            <a href="'.route('order.detail', ['id' => $data['id']]) .'">
                                <i class="fa fa-folder-open"></i>
                            </a>
                        </td>
                    </tr>
                ';
            }
        }
        elseif($request->id_customer == 0){
            $datas = Order::whereDATE('created_at', 'like', '%'.$date.'%')->get();
            foreach($datas as $data){
                $show_data .= '
                    <tr>
                    <td>'.$data->getNameCustomer->name.'</td>
                        <td>'.$data['total_amount'].'</td>
                        <td>'.number_format($data['total_price'], 0, ',', '.').'</td>
                        <td>'.$data['created_at'].'</td>
                        <td>
                            <a href="'.route('order.detail', ['id' => $data['id']]) .'">
                                <i class="fa fa-folder-open"></i>
                            </a>
                        </td>
                    </tr>
                ';
            }
        }

        return $show_data;
    }

    public function product()
    {
        $customer_datas = Customer::All();

        return view('revenue/product', [
            'customer_datas' => $customer_datas,
        ]);
    }

    public function showproduct(Request $request)
    {
        $show_data = '';
        $date = $request->month;
        $id_customer    = $request->id_customer;
        if($request->day != null){
            $date = $request->month.'-'.$request->day;
        }
        $datas = DB::table('products')
                    ->select(DB::raw('products.price_product,products.name_product, SUM(order_details.amount) as total_amount'))
                    ->leftjoin('order_details', 'products.id', '=', 'order_details.product_id')
                    ->leftjoin('orders', 'order_details.order_id', '=', 'orders.id')
                    ->leftjoin('customers', 'orders.customer_id', '=', 'customers.id')
                    ->where('customers.id', '=', $id_customer)
                    ->where('orders.status', '=', '1')
                    ->whereDATE('order_details.created_at', 'like', '%10%')
                    ->groupby('order_details.product_id')
                    ->groupby('products.name_product')
                    ->groupby('products.price_product')
                    ->get();
        foreach ($datas as $data)
        {
            $show_data .= '
                <tr>
                    <td>'.$data->name_product.'</td>
                    <td>'.$data->total_amount.'</td>
                    <td>'.number_format($data->total_amount*$data->price_product, 0, ',', '.').'</td>
                </tr>
            ';
        }
        return $show_data;
    }
}
