<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

use App\Category;

class CategoryController extends Controller
{
    public function index()
    {
        $category   = new Category();
        $categories = $category->getCategory();
        return view('/category/index', [
            'categories' => $categories,
        ]);
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_name'      => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('/category')->withErrors($validator)->withInput();
        }
        else {
           
            $dataInsert = [
                'name_category' => request('category_name'),
            ];
            Category::create($dataInsert);

            return redirect('/category');
        }
    }

    public function delete($id)
    {
        Category::destroy($id);
        return redirect('/category');
    }
}
