<?php

namespace App\Http\Controllers;

use App\Cartimport;
use App\Importprodetail;
use App\Importproduct;
use App\Product;
use Illuminate\Http\Request;
use Session;

class ImportController extends Controller
{
    public function index()
    {
        $getImports = Importproduct::all();
        return view('import.index', [
            'getImports' => $getImports,
        ]);
    }

    public function viewdetail($id)
    {
        $datas = Importprodetail::where(['importprodetail_id' => $id])->get();
        return view('import.viewdetail', [
            'datas' => $datas,
        ]);
    }
    public function create()
    {
        $getProducts = Product::all();
        return view('import.create', [
            'getProducts' => $getProducts,
        ]);
    }

    public function store(Request $request)
    {
        $price = '';
        if ($request->import_new_price) {
            $price = $request->import_new_price;
        }
        $output      = '';
        $total_price = '';
        $id          = $request->product_id;
        $quantity    = $request->quantity;
        $product     = Product::where(['id' => $id])->get();
        $cartimport  = new Cartimport();
        $cartimport->addCartimport($id, $product, $quantity, $price);
        $datas      = Session::get('cartimport');
        foreach($datas as $data){
            $total_price = $data['amount']*$data['wholesale_price'];
            $output .=
                '<tr>
                <td>'.$data['product_name'].'</td>
                <td>'.$data['amount'].'</td>
                <td>'.number_format($total_price, 0, ',', '.').'</td>
                <td><i class="far fa-trash-alt" onclick="myFunction('.$data['id_product'].')"></i></td>
            </tr>';
        }
        return $output;
    }

    public function delete($id)
    {
        $output      = '';
        $cartimport  = new Cartimport();
        $cartimport->destroyCartimport($id);
        $datas       = Session::get('cartimport');
        foreach($datas as $data){
            $total_price = $data['amount']*$data['wholesale_price'];
            $output     .=
                '<tr>
                <td>'.$data['product_name'].'</td>
                <td>'.$data['amount'].'</td>
                <td>'.number_format($total_price, 0, ',', '.').'</td>
                <td><i class="far fa-trash-alt" onclick="myFunction('.$data['id_product'].')"></i></td>
            </tr>';
        }
        return $output;
    }

    public function finish(Request $request)
    {
            $total_amount   = $total_price = $total = 0;
            $data_carts     = Session::get('cartimport');
            if($data_carts){
                foreach($data_carts as $data_cart){
                    $total_amount += $data_cart['amount'];
                    $total_price  += $data_cart['wholesale_price'];
                    $total        += $data_cart['wholesale_price']*$data_cart['amount'];
                }

                $dataInsert = [
                    'note'         => $request->note,
                    'quantity' => $total_amount,
                    'total_price'  => $total,
                ];
                $importProduct = Importproduct::insertGetId($dataInsert);

                foreach($data_carts as $data_cart){
                    $total_price = $data_cart['amount']*$data_cart['wholesale_price'];
                    $dataInsertDetail = [
                        'importprodetail_id' => $importProduct,
                        'product_id'         => $data_cart['id_product'],
                        'quantity'           => $data_cart['amount'],
                        'price'              => $total_price,
                    ];
                    Importprodetail::create($dataInsertDetail);
                }
                Session::forget('cartimport');
                return redirect('import');
            }
            else{
                Session::flash('error_create_order', 'Please Create Product After Finish.');
                return redirect('/order/create');
            }
    }

    public function deleteImportproduct($id)
    {
        Importproduct::where('id', $id)->delete();
        $dataImportprodetails = Importprodetail::where('importprodetail_id', $id)->get();
        foreach ($dataImportprodetails as $dataImportprodetail)
        {
            Importprodetail::where(['importprodetail_id'=> $dataImportprodetail['importprodetail_id']])->delete();
        }
        return redirect('import');
    }
}
