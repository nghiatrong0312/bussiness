<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

use App\Customer;
use App\Order;

class CustomerController extends Controller
{
    public function index()
    {
        $customers = Customer::all();

        return view('customer/index', [
            'customers' => $customers
        ]);
    }

    public function view($id)
    {
        $datas          = Order::where(['customer_id' => $id])->get();
        $customer_names = Customer::where(['id' => $id])->get(); 
        $name           = $customer_names['0']['name'];

        return view('customer/view',[
            'datas' => $datas,
            'name' => $name,
        ]);
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'       => 'required',
            'address'    => 'required',
            'phone'      => 'required|Numeric',
        ]);
        if ($validator->fails()) {
            return redirect('customer')->withErrors($validator)->withInput();
        }
        else {
           
            $dataInsert = [
                'name'      => request('name'),
                'address'   => request('address'),
                'phone'     => request('phone'),
            ];
            Customer::create($dataInsert);

            return redirect('customer');
        }
    }

    public function update(Request $request, $id)
    {
        $dataUpdate = [
            'name'    => request('name'),
            'address' => request('address'),
            'phone'   => request('phone'),
        ];
        Customer::where(['id' => $id])->update($dataUpdate);
    }
}
