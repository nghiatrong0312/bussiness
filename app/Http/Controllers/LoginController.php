<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index()
    {
        return view('login');
    }

    public function check(Request $request)
    {
        
        $rules = [
            'username' =>'required|string',
            'pass' => 'required|min:8'
            ];
        $messages = [
            'username.required' => 'Email là trường bắt buộc',
            'pass.required' => 'Mật khẩu là trường bắt buộc',
            'pass.min' => 'Mật khẩu phải chứa ít nhất 8 ký tự',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        
        
        if ($validator->fails()) {
            // Điều kiện dữ liệu không hợp lệ sẽ chuyển về trang đăng nhập và thông báo lỗi
            Session::flash('note', 'Dang Nhap Khong Thanh Cong !');
            return redirect('/login')->withErrors($validator)->withInput();
        }
        else {
            // Nếu dữ liệu hợp lệ sẽ kiểm tra trong csdl
            
            $username = $request->input('username');
            $password = $request->input('pass');
     
            if( Auth::attempt(['name'   => $username, 'password' => $password])) {
                // Kiểm tra đúng email và mật khẩu sẽ chuyển trang
                echo 'good';
            } else {
                // Kiểm tra không đúng sẽ hiển thị thông báo lỗi
                Session::flash('error', 'Email hoặc mật khẩu không đúng!');
                return redirect('/login');
            }
        }
    }
}
