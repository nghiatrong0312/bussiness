<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

use App\Product;


class ProductController extends Controller
{
    public function index()
    {

        $product      = new Product();
        $products     = $product->getProducts();
        $categories   = $product->getCategories();

        return view('/product/index', [
            'products'    => $products,
            'categories' => $categories,
        ]);
    }

    public function create(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'product_name'      => 'required',
            'product_price'     => 'required|integer',
            'wholesale_price'   => 'required|integer',
            'category_id'       => 'required|integer',
        ]);

        if ($validator->fails()) {
            return redirect('/product')->withErrors($validator)->withInput();
        }
        else {

            $dataInsert = [
                'wholesale_price' => request('wholesale_price'),
                'name_product'    => request('product_name'),
                'price_product'   => request('product_price'),
                'category_id'     => request('category_id'),
            ];
            Product::create($dataInsert);

            return redirect('/product');
        }
    }

    public function edit($id)
    {
        $products = new Product();
        $product  = $products->getProductsbyID($id);

        return view('/product/edit', [
            'product' => $product
        ]);
    }

    public function update(Request $request, $id)
    {
        $dataUpdate = [
            'name_product'  => request('product_name'),
            'price_product' => request('product_price'),
            'category_id'   => request('category'),
        ];
        Product::where(['id' => $id])->update($dataUpdate);

        return redirect('/product');
    }

    public function delete($id)
    {
        Product::destroy($id);
        return redirect('/product');
    }
}
