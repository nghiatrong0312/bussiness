<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

use App\Customer;
use App\Product;
use App\Cart;
use App\Order_Detail;
use App\Order;
use Session;

class OrderController extends Controller
{
    public function index()
    {
        $data_orders = Order::where(['status' => null])->get();

        return view('order/index',[
            'data_orders' => $data_orders,
        ]);
    }

    public function history()
    {
        $data_orders = Order::where(['status' => 1])->get();

        return view('order/history',[
            'data_orders' => $data_orders,
        ]);
    }

    public function create()
    {
        $customers = Customer::All();
        $products  = Product::All();
        return view('order/create', [
            'customers' => $customers,
            'products'  => $products,
        ]);
    }

    public function addproduct(Request $request)
    {
        $price = '';
        if ($request->new_price) {
            $price = $request->new_price;
        }
        $output     = '';
        $total_price = '';
        $id         = $request->product_id;
        $amount     = $request->amount;
        $product    = Product::where(['id' => $id])->get();
    	$cart       = new Cart();
        $cart->addCart($id, $product, $amount, $price);
        $datas      = Session::get('cart');
        foreach($datas as $data){
            $total_price = $data['amount']*$data['product_price'];
            $output .=
            '<tr>
                <td>'.$data['product_name'].'</td>
                <td>'.$data['amount'].'</td>
                <td>'.number_format($total_price, 0, ',', '.').'</td>
                <td><i class="far fa-trash-alt" onclick="myFunction('.$data['id_product'].')"></i></td>
            </tr>';
        }
        return $output;

    }

    public function delete($id)
    {
        $output      = '';
        $cart        = new Cart();
        $cart->destroyCart($id);
        $datas       = Session::get('cart');
        foreach($datas as $data){
            $total_price = $data['amount']*$data['product_price'];
            $output     .=
            '<tr>
                <td>'.$data['product_name'].'</td>
                <td>'.$data['amount'].'</td>
                <td>'.number_format($total_price, 0, ',', '.').'</td>
                <td><i class="far fa-trash-alt" onclick="myFunction('.$data['id_product'].')"></i></td>
            </tr>';
        }
        return $output;
    }

    public function deleteorder($id)
    {
        Order::destroy($id);
        return redirect('order');
    }

    public function finish(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_customer'       => 'required|integer',
        ]);
        if ($validator->fails()) {
            return redirect('/order/create')->withErrors($validator)->withInput();
        }
        else{
            $total_amount   = $total_price = $total = 0;
            $data_carts     = Session::get('cart');
            if($data_carts){
                foreach($data_carts as $data_cart){
                    $total_amount += $data_cart['amount'];
                    $total_price  += $data_cart['product_price'];
                    $total        += $data_cart['product_price']*$data_cart['amount'];
                }

                $dataInsert = [
                    'customer_id'  => $request->id_customer,
                    'total_amount' => $total_amount,
                    'total_price'  => $total,
                ];
                $order = Order::insertGetId($dataInsert);

                foreach($data_carts as $data_cart){
                    $dataInsertDetail = [
                        'order_id'      => $order,
                        'product_id'    => $data_cart['id_product'],
                        'amount'        => $data_cart['amount'],
                        'price'         => $data_cart['product_price'],
                    ];
                    Order_Detail::create($dataInsertDetail);
                }
                Session::forget('cart');
                return redirect('order');
            }
            else{
                Session::flash('error_create_order', 'Please Create Product After Finish.');
                return redirect('/order/create');
            }
        }
    }

    public function detail($id)
    {

        $datas = Order_Detail::where(['order_id' => $id])->get();

        return view('order/detail', [
            'datas' => $datas,
            'id'    => $id,
        ]);
    }

    public function status($id)
    {
        $dataUpdate = [
            'status' => 1,
        ];
        Order::where(['id' => $id])->update($dataUpdate);

        return redirect('order');
    }
}
