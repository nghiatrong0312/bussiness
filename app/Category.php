<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Product;

class Category extends Model
{
    protected $table = 'categories';
    protected $guarded = [];

    public function getCategory()
    {
        return Category::All();
    }
    public function getProductbyID()
    {
        return $this->hasMany(Product::class, 'id', 'category_id');
    }
}
