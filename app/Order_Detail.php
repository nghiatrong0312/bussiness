<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Product;

class Order_Detail extends Model
{
    protected $table = 'order_details';
    protected $guarded = [];

    public function getNameProduct()
    {
        return $this->belongsto(Product::class, 'product_id', 'id');
    }
}
