<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Order;
use App\Order_Detail;

class Customer extends Model
{
    protected $table = 'customers';
    protected $guarded = [];

    public function getOrder()
    {
        return $this->hasMany(Order::class, 'customer_id', 'id');
    }
}
