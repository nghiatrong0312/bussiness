@extends('welcome')
@section('navbar')
@section('content')
<div class="content-wrapper">
    <div class="container">
        <div class="product_title">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
                  <li class="breadcrumb-item"><a href="{{ route('order.index') }}">Order</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Details</li>
                </ol>
            </nav>
        </div>
        <div class="product_table">
            <table class="table table-striped">
                <thead>
                  <tr>
                    <th>Name Product</th>
                    <th>Amount</th>
                    <th>Price</th>
                    
                    <th></th>
                  </tr>
                </thead>    
                <tbody>
                    <?php $total_price   = 0 ?>
                    <?php $total_amount  = 0 ?>
                    @foreach ($datas as $data)
                    <?php $total_price  += $data['price']*$data['amount'] ?>
                    <?php $total_amount += $data['amount'] ?>
                    <tr>
                        <td>{{ $data->getNameProduct->name_product }}</td>
                        <td>{{ $data['amount'] }}</td>
                        <td>{{ number_format($data['price']*$data['amount'], 0, ',', '.')}}</td>
                        <td>
                            <a href=""><i class="far fa-trash-alt"></i></a> 
                        </td>
                    </tr>
                    @endforeach
                    <tr class="table_footer">
                        <th scope="row">Total</th>
                        <td>{{ $total_amount }}</td>
                        <td>{{ number_format($total_price, 0, ',', '.')}}</td>
                        <td>
                            <a href="{{route('order.status', ['id' => $id])}}"><i class="fas fa-shipping-fast"></i></a></td>
                      </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection