@extends('welcome')
@section('navbar')
@section('content')
<div class="content-wrapper">
    <div class="container">
        <div class="product_title">
        <h2>History Order /<small><a href="" class="back"> Back</a></small> </h2>
        </div>

        <div class="product_table">
            <table class="table table-striped">
                <thead>
                  <tr>
                    <th>Customer Name</th>
                    <th>Total Amount</th>
                    <th>Total Price</th>
                    <th>Date to Create</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($data_orders as $data_order)
                    <tr>
                        <td>{{ $data_order->getNameCustomer->name }}</td>
                        <td>{{ $data_order['total_amount'] }}</td>
                        <td>{{ number_format($data_order['total_price'], 0, ',', '.')}}</td>
                        <td>{{ $data_order['created_at'] }}</td>
                        <td>
                            <a href="{{ route('order.detail', ['id' => $data_order['id']]) }}"><i class="fa fa-folder-open"></i></a>
                            <a href="{{ route('order.deleteorder', ['id' => $data_order['id']]) }}"><i class="far fa-trash-alt"></i></a> 
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        
    </div>
</div>
@endsection