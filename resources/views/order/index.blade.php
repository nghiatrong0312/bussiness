@extends('welcome')
@section('navbar')
@section('content')
<div class="content-wrapper">
    <div class="container">
        <div class="product_title">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Orders</li>
                </ol>
            </nav>
        </div>

        <div class="product_form">
            <a href="{{ route('order.create') }}"><button id="submit_create" type="submit" class="btn btn-primary mt-3 create">Tao Don Hang</button></a>
        </div>

        <div class="product_table">
            <table class="table table-striped">
                <thead>
                  <tr>
                    <th>Ten Khach Hang</th>
                    <th>Tong San Pham</th>
                    <th>Ngay Dat Hang</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($data_orders as $data_order)
                    <tr>
                        <td>{{ $data_order->getNameCustomer->name }}</td>
                        <td>{{ $data_order['total_amount'] }}</td>
                        <td>{{ $data_order['created_at'] }}</td>
                        <td>
                            <a href="{{ route('order.detail', ['id' => $data_order['id']]) }}"><i class="fa fa-folder-open"></i></a>
                            <a href="{{ route('order.deleteorder', ['id' => $data_order['id']]) }}"><i class="far fa-trash-alt"></i></a> 
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        
    </div>
</div>
@endsection