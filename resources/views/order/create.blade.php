@extends('welcome')
@section('navbar')
@section('content')
<div class="content-wrapper">

    <div class="container form_create_order">
        <div class="product_title">
            <h2>Customer /<small><a href="" class="back"> Back</a></small> </h2>
        </div>
        @if ( Session::has('error_create_order') )
        <p class="help is-danger">{{ Session::get('error_create_order') }}</p>
        @endif
        <p class="help is-danger">{{ $errors->first('id_customer') }}</p>
        <form method="POST" action="{{ route('order.finish') }}">
            @csrf
            <div class="row">
                <div class="col-sm-12 form_create_order__input">
                    <label for="">Ten Khach Hang</label>
                    <select class="form-control" id="id_customer" name="id_customer" >
                        <option>Chon Ten Khach Hang</option>
                        @foreach ($customers as $customer)
                        <option value="{{ $customer['id'] }}">{{ $customer['name'] }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-sm-6 form_create_order__input">
                    <label for="">Ten San Pham</label>
                    <select class="form-control" id="product">
                        <option>Chon San Pham</option>
                        @foreach ($products as $product)
                        <option value="{{ $product['id'] }}">{{ $product['name_product'] }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-sm-2 form_create_order__input">
                    <label for="">So Luong</label>
                    <input type="number" placeholder="So Luong" id="amount">
                </div>
                <div class="col-sm-1 form_create_order__input">
                    <label for="">Active</label>
                    <input type="checkbox" id="active">
                </div>
                <div class="col-sm-3 form_create_order__input">
                    <label for="">Gia Moi</label>
                    <input type="number" placeholder="Nhap Gia" id="new_price" disabled>
                </div>
                <div class="col-sm-3 form_create_order__input">
                    <a href=""><button type="button" class="btn btn-secondary" id="add_product">Them San Pham</button></a>                    
                </div>
            </div>
            <div class="product_table">
                <table class="table table-striped" >
                    <thead>
                      <tr>
                        <th>Ten San Pham</th>
                        <th>So Luong</th>
                        <th>Gia</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody id="test">
                    </tbody>
                </table>
            </div>
            <div class="form_create_order__button_order">
                <a><button type="submit" class="btn btn-secondary">Hoan Tat</button></a>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript" >
    $(document).ready(function(){
        $('#active').change(function(){
            if($(this).is(':checked')){
                $("#new_price").prop('disabled', false);
            } else {
                $("#new_price").prop('disabled', true);
                $("#new_price").val(null);
            }
        });
        $('#add_product').click(function(event){
            event.preventDefault();
            $("#id_customer").prop('disabled', false);
            $.ajax({
                url:'{{ route('order.addproduct') }}',
                type:'post',
                dataType: 'html',
                data : {
                    "_token": "{{ csrf_token() }}",
                    product_id:$('#product').val(),
                    amount:$('#amount').val(),
                    new_price:$('#new_price').val(),
                },
            }).done(function(data){
                $('#test').html(data);
                $("#amount").val(null);
            });
        });
        setInterval(function(){
            $('#test').load('{{ route('order.addproduct') }}').fadeIn('slow');
        }, 5000);

    });
    function myFunction(id) {
        $.ajax({
                url:'<?php echo url('/order/create/delete') ?>/' + id,
                type:'get',
        }).done(function(data){
            $('#test').html(data);
        });   
    }
</script>
@endsection