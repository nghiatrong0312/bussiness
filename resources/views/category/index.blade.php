@extends('welcome')
@section('navbar')
@section('content')
<div class="content-wrapper">
    <div class="container">
        <div class="product_title">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Categories</li>
                </ol>
            </nav>
        </div>
        <div class="product_form">
            <div class="product_form-title">
                <h6>Create Category</h6>
            </div>
            <form method="POST" action="{{ route('category.create') }}">
            @csrf
            <div class="row">
                <div class="col">
                <input type="text" class="form-control" id="name" placeholder="Category Name" name="category_name">
                <p class="help is-danger">{{ $errors->first('category_name') }}</p>
                </div>
                {{-- <div class="col">
                <input type="text" class="form-control" placeholder="Product Price" name="product_price">
                <p class="help is-danger">{{ $errors->first('product_price') }}</p>
                </div> --}}
            </div>
            <button type="submit" class="btn btn-primary mt-3 create">Creat Category</button>
            </form>
        </div>
        
        <div class="product_table">
            <table class="table table-striped">
                <thead>
                  <tr>
                    <th>Category Name</th>
                    <th>Date to Update</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($categories as $category)
                    <tr>
                        <td>{{$category['name_category']}}</td>
                        <td>{{$category['updated_at']}}</td>
                        <td>
                            <a href="{{route('category.delete', ['id' => $category['id']])}}"><i class="far fa-trash-alt"></i></a> 
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        
    </div>
</div>
@endsection