@extends('welcome')
@section('navbar')
@section('content')
    <div class="content-wrapper">
        <div class="container">
            <div class="product_title">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Đơn Hàng Hăng Ngày</li>
                    </ol>
                </nav>
            </div>

            <div class="product_form">
                <a href="{{ route('import.create') }}"><button id="submit_create" type="submit" class="btn btn-primary mt-3 create">Tạo Đơn Hàng</button></a>
            </div>

            <div class="product_table">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Tổng Sản Phẩm</th>
                        <th>Tổng Giá</th>
                        <th>Ngày Nhập Hàng</th>
                        <th>Ghi Chú</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($getImports as $getImport)
                        <tr>
                            <td>{{$getImport['quantity']}}</td>
                            <td>{{number_format($getImport['total_price'], 0, ',', '.')}}</td>
                            <td>{{$getImport['created_at']}}</td>
                            <td>{{$getImport['note']}}</td>
                            <td>
                                <a href="{{route('import.viewdetail', ['id' => $getImport['id']])}}"><i class="fa fa-folder-open"></i></a>
                                <a href="{{route('import.delete', ['id' => $getImport['id']])}}"><i class="far fa-trash-alt"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>
@endsection
