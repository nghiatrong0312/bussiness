@extends('welcome')
@section('navbar')
@section('content')
    <div class="content-wrapper">

        <div class="container form_create_order">
            <div class="product_title">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('import') }}">Đơn Hàng Hăng Ngày</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Tạo Đơn Hàng</li>
                    </ol>
                </nav>
            </div>
            @if ( Session::has('error_create_order') )
                <p class="help is-danger">{{ Session::get('error_create_order') }}</p>
            @endif
            <p class="help is-danger">{{ $errors->first('id_customer') }}</p>
            <form method="POST" action="{{ route('import.finish') }}">
                @csrf
                <div class="row">
                    <div class="col-sm-6 form_create_order__input">
                        <label for="">Ten San Pham</label>
                        <select class="form-control" id="import_product">
                            <option>Chon San Pham</option>
                            @foreach ($getProducts as $getProduct)
                                <option value="{{ $getProduct['id'] }}">{{ $getProduct['name_product'] }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-2 form_create_order__input">
                        <label for="">So Luong</label>
                        <input type="number" placeholder="So Luong" id="import_amount">
                    </div>
                    <div class="col-sm-1 form_create_order__input">
                        <label for="">Active</label>
                        <input type="checkbox" id="active">
                    </div>
                    <div class="col-sm-3 form_create_order__input">
                        <label for="">Gia Moi</label>
                        <input type="number" placeholder="Nhap Gia" id="import_new_price" disabled>
                    </div>
                    <div class="col-sm-12 form_create_order__input">
                        <label for="">Ghi Chú</label>
                        <input type="text" name="note" placeholder="Ghi Chú">
                    </div>
                    <div class="col-sm-3 form_create_order__input">
                        <a href=""><button type="button" class="btn btn-secondary" id="add_product">Thêm Sản Phẩm</button></a>
                    </div>

                </div>
                <div class="product_table">
                    <table class="table table-striped" >
                        <thead>
                        <tr>
                            <th>Tên Sản Phẩm</th>
                            <th>Số Lượng</th>
                            <th>Giá</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody id="test">
                            @if(Session::get('cartimport'))
                                @foreach(Session::get('cartimport') as $product)
                                    <?php
                                        $total_price = 0;
                                        $total_price = $product['amount']*$product['product_price'];
                                    ?>
                                    <tr>
                                        <td>{{$product['product_name']}}</td>
                                        <td>{{$product['amount']}}</td>
                                        <td>{{number_format($total_price, 0, ',', '.')}}</td>
                                        <td><i class="far fa-trash-alt" onclick="myFunction({{$product['id_product']}})"></i></td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="form_create_order__button_order">
                    <a><button type="submit" class="btn btn-secondary">Hoan Tat</button></a>
                </div>
            </form>
        </div>
    </div>
    <script type="text/javascript" >
        $(document).ready(function(){
            $('#active').change(function(){
                if($(this).is(':checked')){
                    $("#import_new_price").prop('disabled', false);
                } else {
                    $("#import_new_price").prop('disabled', true);
                    $("#import_new_price").val(null);
                }
            });
            $('#add_product').click(function(event){
                event.preventDefault();
                $.ajax({
                    url:'{{ route('import.create-store') }}',
                    type:'post',
                    dataType: 'html',
                    data : {
                        "_token": "{{ csrf_token() }}",
                        product_id:$('#import_product').val(),
                        quantity:$('#import_amount').val(),
                        new_price:$('#import_new_price').val(),
                    },
                }).done(function(data){
                    $('#test').html(data);
                    $("#amount").val(null);
                });
            });


        });
        function myFunction(id) {
            $.ajax({
                url:'<?php echo url('/import/create/delete') ?>/' + id,
                type:'get',
            }).done(function(data){
                $('#test').html(data);
            });
        }
    </script>
@endsection
