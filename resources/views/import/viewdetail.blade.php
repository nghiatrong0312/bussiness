@extends('welcome')
@section('navbar')
@section('content')
    <div class="content-wrapper">
        <div class="container">
            <div class="product_title">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('import') }}">Đơn Hàng Hằng Ngày</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Details</li>
                    </ol>
                </nav>
            </div>
            <div class="product_table">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Name Product</th>
                        <th>Amount</th>
                        <th>Price</th>

                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $total_price   = 0 ?>
                    <?php $total_amount  = 0 ?>
                    @foreach ($datas as $data)
                        <tr>
                            <td>{{ $data->getNameProduct->name_product }}</td>
                            <td>{{ $data['quantity'] }}</td>
                            <td>{{ number_format($data['price'], 0, ',', '.')}}</td>
                            <td>
                                <a href=""><i class="far fa-trash-alt"></i></a>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
