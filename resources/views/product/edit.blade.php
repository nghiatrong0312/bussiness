@extends('welcome')
@section('navbar')
@section('content')
<div class="content-wrapper">
    <div class="container">
        <div class="product_title">
        <h2>Product /<small><a href="" class="back"> Back</a></small> </h2>
        </div>
        <div class="product_form">
            <div class="product_form-title">
                <h6>Edit Product</h6>
            </div>
            <form method="POST" action="{{ route('product.update', ['id' => $product->id]) }}">
            @csrf
            <div class="row">
                <div class="col">
                    <label for="">Name Product</label>
                    <input type="text" class="form-control" id="name" placeholder="" name="product_name" value="{{ $product->name_product }}">
                    <p class="help is-danger">{{ $errors->first('product_name') }}</p>
                </div>
                <div class="col">
                    <label for="">Price Product</label>
                    <input type="text" class="form-control" placeholder="Product Price" name="product_price" value="{{$product->price_product}}">
                    <p class="help is-danger">{{ $errors->first('product_price') }}</p>
                </div>
                <div class="col">
                    <label for="">Wholesale Product</label>
                    <input type="text" class="form-control" placeholder="Product Price" name="wholesale_price" value="{{$product->wholesale_price}}">
                    <p class="help is-danger">{{ $errors->first('product_price') }}</p>
                </div>
            </div>
            <button type="submit" class="btn btn-primary mt-3 create">Update Product</button>
            </form>
        </div>
    </div>
</div>
@endsection