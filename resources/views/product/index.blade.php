@extends('welcome')
@section('navbar')
@section('content')
<div class="content-wrapper">
    <div class="container">
        <div class="product_title">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Products</li>
                </ol>
            </nav>
        </div>
        <div class="product_form">
            <div class="product_form-title">
                <h6>Create Product</h6>
            </div>
            <form method="POST" action="{{ route('product.create') }}">
            @csrf
            <div class="form-group row">
                <div class="col">
                    <input type="text" class="form-control" id="name" placeholder="Product Name" name="product_name">
                    <p class="help is-danger">{{ $errors->first('product_name') }}</p>
                </div>
                <div class="col">
                    <input type="text" class="form-control" placeholder="Buyin Price" name="wholesale_price">
                    <p class="help is-danger">{{ $errors->first('product_price') }}</p>
                </div>
                <div class="col">
                    <input type="text" class="form-control" placeholder="Sale Price" name="product_price">
                    <p class="help is-danger">{{ $errors->first('product_price') }}</p>
                </div>
                
                <div class="col">
                    <select class="form-control" id="exampleFormControlSelect1" name="category_id">
                        <option>Select Category</option>
                        @foreach ($categories as $category)
                            <option value="{{$category['id']}}">{{$category['name_category']}}</option>
                        @endforeach                        
                    </select>
                    <p class="help is-danger">{{ $errors->first('category_id') }}</p>
                </div>
            </div>
            <button type="submit" class="btn btn-primary mt-3 create">Creat Product</button>
            </form>
        </div>
        
        <div class="product_table">
            <table class="table table-striped">
                <thead>
                  <tr>
                    <th>Product Name</th>
		    <th>Buyin Price</th>
                    <th>Sale Price</th>
                    <th>Category</th>
                    <th>Date to Update</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($products as $product)
                        <tr>
                            <td>{{ $product['name_product'] }}</td>
			    <td>{{ number_format($product['wholesale_price'], 0, ',', '.')}} vnd</td>
                            <td>{{ number_format($product['price_product'], 0, ',', '.')}} vnd</td>
                            <td>{{ $product->getCategorybyID->name_category }}</td>
                            <td>{{ $product['updated_at'] }}</td>
                            <td>
                                <a href="{{route('product.edit', ['id' => $product['id']])}}"><i class="fa fa-edit"></i></a>
                                <a href="{{route('product.delete', ['id' => $product['id']])}}"><i class="far fa-trash-alt"></i></a> 
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        
    </div>
</div>
@endsection
