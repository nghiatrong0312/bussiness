@extends('welcome')
@section('navbar')
@section('content')
<div class="content-wrapper">
    <div class="container">
        <div class="product_title">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Customer</li>
                </ol>
            </nav>
        </div>
        
        <div class="product_form">
            <div class="product_form-title">
                <h6>Create Customer</h6>
            </div>
            <form method="POST" action="{{ route('customer.create') }}" id="update_customer">
            @csrf
            <div class="row">
                <div class="col">
                    <input type="text" class="form-control" id="name" placeholder="Name" name="name">
                    <p class="help is-danger">{{ $errors->first('name') }}</p>
                </div>
                <div class="col">
                    <input type="text" class="form-control" id="address" placeholder="Address" name="address">
                    <p class="help is-danger">{{ $errors->first('address') }}</p>
                </div>
                <div class="col">
                    <input type="text" class="form-control" id="phone" placeholder="Phone " name="phone">
                    <p class="help is-danger">{{ $errors->first('phone') }}</p>
                </div>
            </div>
            <button id="submit_create" type="submit" class="btn btn-primary mt-3 create">Creat Customer</button>
            <button style="display: none" id="submit_update" type="submit" class="btn btn-primary mt-3 create">Update Customer</button>
            </form>
        </div>
        
        <div class="product_table">
            <table class="table table-striped">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Phone</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($customers as $customer)
                    <tr>
                        <td>{{ $customer['name'] }}</td>
                        <td>{{ $customer['address'] }}</td>
                        <td>(+84) {{ $customer['phone'] }}</td>
                        <td>
                            <a href="{{ route('customer.view', ['id' => $customer['id']]) }}"><i class="fa fa-folder-open"></i></a>
                            <a href="" id="edit{{ $customer['id'] }}"><i class="fa fa-edit"></i></a>
                            <a href=""><i class="far fa-trash-alt"></i></a> 
                        </td>
                    </tr>
                    @endforeach                    
                </tbody>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        <?php foreach ($customers as $key => $customer): ?>
        $('#edit{{ $customer['id'] }}').click(function(event){
            event.preventDefault();
            $('#submit_create').hide();
            $('.fa-edit').hide();
            $('#submit_update').show(200);
            $('#name').focus();
            $('#name').val('<?php echo $customer['name'] ?>');
            $('#address').val('<?php echo $customer['address'] ?>');
            $('#phone').val('<?php echo $customer['phone'] ?>');

            $('#update_customer').submit(function(event){
            event.preventDefault();
            var form_data_incentives = $(this).serialize();
            $.ajax({
            url:'<?php echo url('customer/update') ?>/' + <?php echo $customer['id'] ?>,
            type:'post',
            dataType: 'json',
            data : form_data_incentives,
            });
            return location.reload();
        });
        });
        <?php endforeach ?>
    });
</script>
@endsection