@extends('welcome')
@section('navbar')
@section('content')
<div class="content-wrapper">
    <div class="container">
        <div class="product_title">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
                  <li class="breadcrumb-item"><a href="{{ route('customer.index') }}">Customer</a></li>
                  <li class="breadcrumb-item active" aria-current="page">{{$name}}</li>
                </ol>
            </nav>
        </div>
        <div class="product_table">
            <table class="table table-striped">
                <thead>
                  <tr>
                    <th>Total Amount</th>
                    <th>Total Price</th>
                    <th>Created</th>
                    <th></th>
                  </tr>
                </thead>    
                <tbody>
                    @foreach ($datas as $data)
                    <tr>
                        <td>{{ $data['total_amount'] }}</td>
                        <td>{{ number_format($data['total_price'], 0, ',', '.')}}</td>
                        <td>{{ $data['created_at'] }}</td>
                        <td>
                            <a href="{{ route('order.detail', ['id' => $data['id']]) }}"><i class="fa fa-folder-open"></i></a> 
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <button type="button" class="btn btn-secondary">Revenue Statistics</button>
        </div>
    </div>
</div>
@endsection