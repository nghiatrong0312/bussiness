@extends('welcome')
@section('navbar')
@section('content')
<div class="content-wrapper">

    <div class="container form_create_order">
        <div class="product_title">
            <h2>Customer /<small><a href="" class="back"> Back</a></small> </h2>
        </div>
        @if ( Session::has('error_create_order') )
        <p class="help is-danger">{{ Session::get('error_create_order') }}</p>
        @endif
        <p class="help is-danger">{{ $errors->first('id_customer') }}</p>
        <form id="revenue_product_search">
            @csrf
            <div class="row">
                <div class="col-sm-3 form_create_order__input">
                    <label for="">Ten Khach Hang</label>
                    <select class="form-control" id="id_customer" name="id_customer" >
                        <option>Chon Khach Hang</option>
                        @foreach ($customer_datas as $customer_data)
                        <option value="{{$customer_data['id']}}">{{$customer_data['name']}}</option>
                        @endforeach
                        
                    </select>
                </div>
                <div class="col-sm-1 form_create_order__input">
                    <label for="">Ngay</label>
                    <input type="day" placeholder="Day" id="day" name="day">
                </div>
                <div class="col-sm-3 form_create_order__input">
                    <label for="">Thang va nam</label>
                    <input type="month" placeholder="Month" id="month" name="month">
                </div>
                <div class="col-sm-3 form_create_order__input">
                    <button type="submit" class="btn btn-secondary" >Search</button>                   
                </div>
            </div>
        </form>    
        <div class="product_table">
            <table class="table table-striped" >
                <thead>
                    <tr>
                        <th>Name Customer</th>
                        <th>Amount</th>
                        <th>Price</th>
                        <th>Creat</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody id="revenue_product">
                   
                </tbody>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript" >
    $(document).ready(function(){
        $('#revenue_product_search').submit(function(event){
            event.preventDefault();
            var data_product_revenue = $(this).serialize();
            $.ajax({
                url:'{{ route('revenue.order_show') }}',
                type:'post',
                dataType: 'html',
                data : data_product_revenue,
            }).done(function(data){
                $('#revenue_product').html(data);
            });
        });
    });
    
</script>
@endsection