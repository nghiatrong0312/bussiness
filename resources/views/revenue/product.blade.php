@extends('welcome')
@section('navbar')
@section('content')
<div class="content-wrapper">

    <div class="container form_create_order">
        <div class="product_title">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{ route('home.index') }}">Home</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Product Revenue</li>
                </ol>
            </nav>
        </div>
        @if ( Session::has('error_create_order') )
        <p class="help is-danger">{{ Session::get('error_create_order') }}</p>
        @endif
        <p class="help is-danger">{{ $errors->first('id_customer') }}</p>
        <form id="revenue_product_search">
            @csrf
            <div class="row">
                <div class="col-sm-3 form_create_order__input">
                    <label for="">Select Customer</label>
                    <select class="form-control" id="id_customer" name="id_customer" >
                        <option>Select Customer</option>
                        @foreach ($customer_datas as $customer_data)
                        <option value="{{$customer_data['id']}}">{{$customer_data['name']}}</option>
                        @endforeach

                    </select>
                </div>
                <div class="col-sm-3 form_create_order__input">
                    <label for="">Month And Year</label>
                    <input type="month" placeholder="Month" id="month" name="month">
                </div>
                <div class="col-sm-3 form_create_order__input">
                    <button type="submit" class="btn btn-secondary" >Search</button>
                </div>
            </div>
        </form>
        <div class="product_table">
            <table class="table table-striped" >
                <thead>
                    <tr>
                        <th>Tên Mặt Hàng</th>
                        <th>Tổng Số Lượng</th>
                        <th>Tổng Giá</th>
                    </tr>
                </thead>
                <tbody id="revenue_product">

                </tbody>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript" >
    $(document).ready(function(){
        $('#revenue_product_search').submit(function(event){
            event.preventDefault();
            var data_product_revenue = $(this).serialize();
            $.ajax({
                url:'{{ route('revenue.product_show') }}',
                type:'post',
                dataType: 'html',
                data : data_product_revenue,
            }).done(function(data){
                $('#revenue_product').html(data);
            });
        });
    });

</script>
@endsection
