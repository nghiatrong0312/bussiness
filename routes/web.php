<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});
Route::get('/', 'HomeController@index')->name('home.index');

Route::get('/login', 'LoginController@index')->name('login.index');
Route::post('/login', 'LoginController@check')->name('login.check');

// ProductController
Route::group(['prefix' => '/product'], function () {
    Route::get('', 'ProductController@index')->name('product.index');
    Route::post('/create', 'ProductController@create')->name('product.create');
    Route::get('/edit/{id}', 'ProductController@edit')->name('product.edit');
    Route::post('/update/{id}', 'ProductController@update')->name('product.update');
    Route::get('/delete/{id}', 'ProductController@delete')->name('product.delete');
});

// CategoryController
Route::group(['prefix' => '/category'], function(){
    Route::get('', 'CategoryController@index')->name('category.index');
    Route::post('/create', 'CategoryController@create')->name('category.create');
    Route::get('/delete/{id}', 'CategoryController@delete')->name('category.delete');
});

// CustomerController
Route::group(['prefix' => '/customer'], function(){
    Route::get('', 'CustomerController@index')->name('customer.index');
    Route::get('/view/{id}', 'CustomerController@view')->name('customer.view');
    Route::post('/create', 'CustomerController@create')->name('customer.create');
    Route::post('/update/{id}', 'CustomerController@update')->name('customer.update');

});

// OrderController
Route::group(['prefix' => '/order'], function(){
    Route::get('', 'OrderController@index')->name('order.index');
    Route::get('/history', 'OrderController@history')->name('order.history');
    Route::get('/delete/{id}', 'OrderController@deleteorder')->name('order.deleteorder');
    Route::get('/create', 'OrderController@create')->name('order.create');
    Route::post('/create/addproduct', 'OrderController@addproduct')->name('order.addproduct');
    Route::get('/create/delete/{id}', 'OrderController@delete')->name('order.delete');
    Route::post('/create/finish', 'OrderController@finish')->name('order.finish');
    Route::get('/detail/{id}', 'OrderController@detail')->name('order.detail');
    Route::get('/status/{id}', 'OrderController@status')->name('order.status');
});

// RevenueController
Route::group(['prefix' => '/revenue'], function(){
    Route::get('/order', 'RevenueController@order')->name('revenue.order');
    Route::get('/product', 'RevenueController@product')->name('revenue.product');
    Route::post('/order/show', 'RevenueController@showorder')->name('revenue.order_show');
    Route::post('/product/show', 'RevenueController@showproduct')->name('revenue.product_show');
});

// RevenueImportController
Route::group(['prefix' => '/revenueimport'], function(){
    Route::get('/', 'RevenueImportController@index')->name('revenueimport');
    Route::post('/show', 'RevenueImportController@show')->name('revenueimport.show');
});

/**
 * ImportController : use to import product
 */
Route::group(['prefix' => '/import'], function(){
    Route::get('/', 'ImportController@index')->name('import');
    Route::get('/create', 'ImportController@create')->name('import.create');
    Route::post('/create/store', 'ImportController@store')->name('import.create-store');
    Route::get('/create/delete/{id}', 'ImportController@delete')->name('import.create-delete');
    Route::post('/create/finish', 'ImportController@finish')->name('import.finish');
    Route::get('/delete/{id}', 'ImportController@deleteImportproduct')->name('import.delete');
    Route::get('/viewdetail/{id}', 'ImportController@viewdetail')->name('import.viewdetail');
});

