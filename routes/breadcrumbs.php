<?php
Breadcrumbs::for('home', function ($trail) {
    $trail->push('Home', route('home.index'));
});

Breadcrumbs::for('customer', function ($trail) {
    $trail->parent('home');
    $trail->push('Customer', route('customer.index'));
});