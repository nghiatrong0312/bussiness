const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('resources/sass/app.scss', 'public/css')
mix.sass('resources/sass/style.scss', 'public/css')
// mix.sass("resources/sass/auth.scss", "public/css")
// mix.sass("resources/sass/member.scss", "public/css")
  .options({
    processCssUrls: false
  });
// mix.js("resources/js/main.js", "public/js")
//     .js("resources/js/auth.js", "public/js")
//     .js("resources/js/validate-email.js", "public/js")
//     .js("resources/js/member.js", "public/js")
mix.version();
mix.copyDirectory("resources/js/*", "public/js")
   .copyDirectory("resources/css/*.css", "public/css")
mix.disableNotifications();
mix.browserSync("http://127.0.0.1:8000/");
